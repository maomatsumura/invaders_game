﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class End : MonoBehaviour {

	#region フィールド - Unity Inspector

	public GameObject highScoreImage;

	public GameObject scoreObj;

	public bool getHighScore = false;

	public Score _Score;

	public GameObject scoreTextObj;

	// スコアを表示する。
	public Text scoreText;
	// ハイスコアを表示する。
	public Text highScoreText;

	public GameObject normalButton;

	public GameObject enterButton;

	#endregion

	#region フィールド

	private int score;

	private int highScore;

	#endregion

	#region メソッド

	void Start ()
	{
		normalButton.gameObject.SetActive(true);
		enterButton.gameObject.SetActive(false);

		//getHighScore = Score.getHighScored ();

		// スコア・ハイスコアを表示する。
//		scoreText.text = score.ToString ();
//		highScoreText.text = highScore.ToString ();

	}

	void Update ()
	{
//		if (getHighScore) 
//		{
//			//SceneManager.LoadScene ("New Records");
//		}

	}			

	#endregion
}
