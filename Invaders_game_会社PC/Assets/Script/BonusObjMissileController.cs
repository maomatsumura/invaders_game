﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BonusObjMissileController : MonoBehaviour {

	#region フィールド　- Unity Inspector

	//敵ミサイルオブジェクトの挙動制御
	public GameObject preBonusObjMissilePrefab;

	public float rateY;

	public Vector3 pos;

	public float moveY;

	public bool isDestroyedObj;

	public GameObject getBonusScore;

	#endregion

	#region フィールド

	#endregion

	#region メソッド

	void Start ()
	{
		rateY = 0.1f;

		isDestroyedObj = false;

		getBonusScore.SetActive (false);
	}

	void Update () 
	{
		pos = transform.localPosition;

		Move ();
	}

	public void Move()
	{
		float limitMoved_Y = -15;

		moveY += rateY * Time.deltaTime;

		if (pos.y > limitMoved_Y) 
		{
			transform.localPosition = new Vector3(gameObject.transform.localScale.x, pos.y - moveY, 0);

			isDestroyedObj = false;
		}
		else
		{
			isDestroyedObj = true;

			DestroyImmediate(gameObject);
			//SetBool ();
		}
	}

//	public void SetBool()
//	{
//		_Enemy = preEnemyPrefab.GetComponent<Enemy> ();
//		_Enemy.IsDestroyedMissileObj = isDestroyedObj;
//	}

	#endregion
}
