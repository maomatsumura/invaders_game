﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Score : MonoBehaviour {

	#region フィールド - Unity Inspector

	// スコアを表示する。
	public Text scoreText;
	// ハイスコアを表示する。
	public Text highScoreText;

	public GameObject NormalScore;

	public GameObject HighScore;

	// スコア。
	public static int score;

	// ハイスコア。
	public static int highScore;

	public static bool getHighScore = false;

	public bool pushedReset = false;

	public GameObject normalButton;

	public GameObject enterButton;

	public AudioClip audioClip1;


	#endregion

	#region フィールド

	// PlayerPrefsで保存するためのキー。
	private string ScoreKey = "Score";

	// PlayerPrefsで保存するためのキー。
	private string highScoreKey = "highScore";

	private bool gameStart = false;

	private AudioSource audioSource;

	#endregion

	void Start ()
	{
		gameStart = Title.getGameStart ();

		if (!gameStart) 
		{
			// スコアを取得する。保存されていなければ0を取得する。
			score = PlayerPrefs.GetInt (ScoreKey, 0);

			// ハイスコアを取得する。保存されていなければ0を取得する。
			highScore = PlayerPrefs.GetInt (highScoreKey, 0);
		}
		else
		{
			Initialize ();
		}
	}

	void Update ()
	{
		// スコアがハイスコアより大きければ
		if (highScore < score || highScore == score) 
		{
			highScore = score;
			getHighScore = true;
		}

		// スコア・ハイスコアを表示する。
		scoreText.text = score.ToString ();
		highScoreText.text = highScore.ToString ();


//		// スコア・ハイスコアを表示する。
//		scoreText.text = score.ToString ();
//		highScoreText.text = highScore.ToString ();
	}

	// ゲーム開始前の状態に戻す。
	private void Initialize ()
	{
		score = 0;
		// スコアを取得する。保存されていなければ0を取得する。
		//score = PlayerPrefs.GetInt (ScoreKey, 0);

		// ハイスコアを取得する。保存されていなければ0を取得する。
		highScore = PlayerPrefs.GetInt (highScoreKey, 0);

		getHighScore = false;
	}

	// ポイントの追加。
	public void AddPoint (int point)
	{
		score = score + point;
	}

	// ハイスコアの保存。
	public void Save ()
	{
		// スコアを保存する。
		PlayerPrefs.SetInt (ScoreKey, score);

		// ハイスコアを保存する。
		PlayerPrefs.SetInt (highScoreKey, highScore);

		PlayerPrefs.Save ();
	}

	public void OnRetryButton()
	{
		audioSource = gameObject.GetComponent<AudioSource>();
		audioSource.clip = audioClip1;
		audioSource.Play ();

		score = 0;

		// ハイスコアを取得する。保存されていなければ0を取得する。
		highScore = PlayerPrefs.GetInt (highScoreKey, 0);

		PlayerPrefs.Save ();

		pushedReset = true;

		normalButton.gameObject.SetActive(false);
		enterButton.gameObject.SetActive(true);

		SceneManager.LoadScene ("Title");
	}

	public void OnResetButton()
	{
		PlayerPrefs.DeleteAll ();
		PlayerPrefs.Save ();

		pushedReset = true;
	}
		
	public static int getScoreNum()
	{
		return score;
	}

	public static int getHighScoreNum()
	{
		return highScore;
	}

	public static bool getHighScored()
	{
		return getHighScore;
	}

	public void getHighScoredbool(bool get)
	{
		getHighScore = get;
	}
}
