﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PlayerController : MonoBehaviour {

	#region フィールド　- Unity Inspector

	public GameObject playerObject;

	public GameObject objectRoot;

	public GameObject player_leastLife;

	public GameObject player_leastLife_root;

	public List<GameObject> least_life;

	public Renderer rend_player;

	public Score _Score;

	public GameObject scoreTextObj;

	public Title _Title;

	public GameObject titleObj;

	public GameObject playerHouse;

	public static bool getLose = false;

	#endregion

	#region フィールド

	private float initializeY;
	private float initializeZ;

	private float moveX_Rate;
	private float moveX_Player;

	private int player_life = 3;

	/// <summary>
	//// Lifeオブジェクトを生成する間隔。
	/// </summary>
	private int space; 

	/// <summary>
	//// Playerオブジェクト点滅間隔。
	/// </summary>
	private float timeCount = 0.4f;

	/// <summary>
	//// Playerオブジェクト点滅用判定。
	/// </summary>
	private bool damaged = false;

	private bool getHighScore;

	#endregion

	#region メソッド

	void Start()
	{
		ValueInitialization ();

		SetPlayerLife ();

		playerObject.transform.localPosition = new Vector3(objectRoot.transform.localPosition.x, initializeY, initializeZ);

		rend_player = GetComponent<Renderer> ();
	}

	void Update()
	{
		MovePlayer ();
		StopPlayer ();

		if (damaged) 
		{
			DamageAction ();
		} 
		else
		{
			return;
		}

		if (timeCount < 0) 
		{
			damaged = false;
			timeCount = 0.4f;
			rend_player.enabled = true;
		}
	}

	public void MovePlayer()
	{
		if (Input.GetKey (KeyCode.LeftArrow))
		{
			moveX_Player -= moveX_Rate * Time.deltaTime;
		}
		if (Input.GetKey (KeyCode.RightArrow))
		{
			moveX_Player += moveX_Rate * Time.deltaTime;
		}

		SetPlayerPosition ();
	}

	public void StopPlayer()
	{
		if (playerObject.transform.localPosition.x <= -140)
		{
			moveX_Player = -140;
			SetPlayerPosition ();
		}
		else if (playerObject.transform.localPosition.x >= 140)
		{
			moveX_Player = 140;
			SetPlayerPosition ();
		}
	}

	public void SetPlayerPosition()
	{
		playerObject.transform.localPosition = new Vector3 (moveX_Player, initializeY, initializeZ);
	}
		
	public void ValueInitialization()
	{
		getLose = false;

		initializeY = -200.0f;
		initializeZ = -47.0f;
		moveX_Rate = 100.0f;
		moveX_Player = 0;
	}

	private void OnTriggerEnter(Collider other)
	{
		int i = 0;

		if (other.gameObject.tag == "EnemyMissile")
		{
			damaged = true;

			player_life--;

			i = player_life;

			Destroy (least_life [i]);

			if (player_life < 1)
			{
				Destroy (gameObject);
			}

			if (player_life == 0)
			{
				_Score = scoreTextObj.GetComponent<Score> ();
				_Score.Save ();

				_Title = titleObj.GetComponent<Title> ();
				_Title.getGameStartbool (false);

				bool gameStart;
				gameStart = Title.getGameStart ();

				getLose = true;

				getHighScore = Score.getHighScored ();

				if (getHighScore) 
				{
					SceneManager.LoadScene ("New Records");
				}
				else
				{
					SceneManager.LoadScene ("End");
				}
			}
		} 
		else
		{
			damaged = false;
		}
	}

	//Enemyミサイルに衝突した際のアクション。
	private void DamageAction()
	{
		rend_player.enabled = !rend_player.enabled;

		timeCount -= Time.deltaTime;

	}

	private void SetPlayerLife()
	{
		int createNumbers = 3;

		for (int life = 0; life < createNumbers; life++)
		{
			space += 30;

			least_life[life] = Instantiate (player_leastLife);
			least_life[life].transform.SetParent (player_leastLife_root.transform);
			least_life[life].transform.localPosition = new Vector3 (50 + space, playerHouse.transform.localPosition.y , 0);
		}
	}

	public static bool getLoseBool()
	{
		return getLose;
	}

	//プレイヤーライフオブジェクトに動きをつけたい。
	private void ActionPlayreLife()
	{
		
	}
		
	#endregion
}
