﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WallController : MonoBehaviour {

	#region フィールド - Unity Inspector

	public Collider wallCollider;

	public Wall _Wall;

	public GameObject _wallObject;

	public bool destroiyedWallObject = false;

	#endregion

	#region フィールド

	private int wallLife;

	#endregion

	#region メソッド

	void Start () 
	{
		wallCollider.isTrigger = true;

		wallLife = 4;
	}

	void Update ()
	{

	}

	private void OnTriggerEnter(Collider other)
	{        
		_Wall = _wallObject.GetComponent<Wall> ();

		if (other.gameObject.tag == "EnemyMissile" )
		{
			wallLife--;

			destroiyedWallObject = true;
			Destroy (gameObject);
		}
	}

	#endregion
}