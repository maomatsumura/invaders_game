﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BonusObjMissile : MonoBehaviour {

	#region フィールド - Unity Inspector

	public Collider bonus_misslePrefab;

	public BonusObj _BonusObj;

	public GameObject bonusObj;

	public GameObject addbonusObj;

	public Score _Score;

	public GameObject scoreTextObj;

	public Renderer rend_bonus;

	public Renderer rend_addBonus;

	#endregion

	#region フィールド

	private GameObject bonus_dammyPrefab;

	/// <summary>
	//// Bonusオブジェクト点滅間隔。
	/// </summary>
	private float timeCount = 0.4f;

	/// <summary>
	//// Bonusオブジェクト点滅用判定。
	/// </summary>
	private bool damaged = false;

	#endregion

	#region メソッド

	void Start ()
	{
		bonus_misslePrefab.isTrigger = true;

	}

	void Update ()
	{
		if (damaged) 
		{
			DamageAction ();
		} 
		else
		{
			return;
		}

		if (timeCount < 0) 
		{
			damaged = false;
			timeCount = 0.4f;
			DestroyImmediate (bonusObj);
			DestroyImmediate (addbonusObj);
		}
	}

	private void OnTriggerEnter(Collider other)
	{
		if(other.gameObject.tag == "Missile")
		{
			damaged = true;

			_Score = scoreTextObj.GetComponent<Score> ();
			_Score.AddPoint (100);

			_BonusObj = bonusObj.GetComponent<BonusObj> ();
			_BonusObj.AddBonusBool (true);

		}
	}

	//Playerミサイルに衝突した際のアクション。
	private void DamageAction()
	{
		rend_bonus.enabled = !rend_bonus.enabled;

		rend_addBonus.enabled = !rend_addBonus.enabled;

		timeCount -= Time.deltaTime;

	}

	#endregion
}
