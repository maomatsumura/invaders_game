﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShotObjectController : MonoBehaviour {

	#region フィールド - Unity Inspector

	public GameObject shotPrefab;

	public GameObject player_Object;

	public AudioClip audioClip1;

	#endregion

	#region フィールド

	private AudioSource audioSource;

	#endregion

	#region メソッド

	void Start ()
	{
		
	}

	void Update ()
	{
		ShotObject ();
	}

	public void ShotObject()
	{
		if (Input.GetKeyDown (KeyCode.UpArrow)) 
		{
			audioSource = gameObject.GetComponent<AudioSource>();
			audioSource.clip = audioClip1;
			audioSource.Play ();

			GameObject shot = Instantiate (shotPrefab);
			shot.transform.SetParent (player_Object.transform.parent);
			shot.transform.localPosition = player_Object.transform.localPosition;
		}

	}

	#endregion
}
