﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EnemyController : MonoBehaviour {

	#region フィールド - Unity Inspector

	public float moveX_Right;

	public float moveX_Left;

	public Collider enemyCollider;

	public Renderer rend_enemy;

	public bool isDestroyedObj;

	public Enemy _Enemy;

	public GameObject preEnemyPrefab;

	public AudioClip audioClip1;

	#endregion

	#region フィールド

	private bool IsMoveRight;

	/// <summary>
	//// Enemyオブジェクト点滅間隔。
	/// </summary>
	private float timeCount = 0.4f;

	/// <summary>
	//// Enemyオブジェクト点滅用判定。
	/// </summary>
	private bool damaged = false;

	private int random_count;

	private AudioSource audioSource;

	#endregion

	#region メソッド

	void Start () 
	{
		enemyCollider.isTrigger = true;

		ValueInitilization ();

		rend_enemy = GetComponent<Renderer> ();

		isDestroyedObj = false;
	}

	void Update ()
	{
		if (IsMoveRight)
		{
			MoveToStartPoint ();
		} 
		if (!IsMoveRight)
		{
			MoveEnemyObject ();
		}

		if (damaged) 
		{
			DamageAction ();
		} 
		else
		{
			return;
		}

		if (timeCount < 0) 
		{
			damaged = false;
			timeCount = 0.4f;
			DestroyImmediate (gameObject);
			isDestroyedObj = true;
			SetBool ();

			_Enemy.SetScoreText ();
		}
	}

	private void MoveEnemyObject()
	{
		++moveX_Left;
		transform.localPosition = new Vector3 (transform.localPosition.x + (moveX_Left * Time.deltaTime), transform.localPosition.y, transform.localPosition.z);

		if (moveX_Left >= 90)
		{
			IsMoveRight = true;
			MoveToStartPoint ();
			moveX_Left = 0;

			MovePositionY ();
		}
	}

	private void MoveToStartPoint()
	{
		++moveX_Right;
		transform.localPosition = new Vector3 (transform.localPosition.x - (moveX_Right * Time.deltaTime), transform.localPosition.y, transform.localPosition.z);

		if (moveX_Right >= 90)
		{
			IsMoveRight = false;
			MoveEnemyObject ();
			moveX_Right = 0;

			MovePositionY ();
		}
	}

	private void MovePositionY()
	{
		Vector3 pos = transform.localPosition;
		pos.y -= 5;
		transform.localPosition = pos;
	}

	private void ValueInitilization()
	{
		moveX_Right = 10f;
		moveX_Left = 10f;

		IsMoveRight = false;
	}

	private void OnTriggerEnter(Collider other)
	{	
		if (other.gameObject.tag == "Missile" )
		{
			audioSource = gameObject.GetComponent<AudioSource>();
			audioSource.clip = audioClip1;
			audioSource.Play ();

			damaged = true;
		}
	}

	//Playerミサイルに衝突した際のアクション。
	private void DamageAction()
	{
		rend_enemy.enabled = !rend_enemy.enabled;

		timeCount -= Time.deltaTime;

	}
		
	public void SetBool()
	{
		_Enemy = preEnemyPrefab.GetComponent<Enemy> ();
		_Enemy.IsDestroyedEnemyObj = isDestroyedObj;
	}

	#endregion

}
