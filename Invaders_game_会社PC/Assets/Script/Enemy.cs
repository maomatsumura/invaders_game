﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Linq;

public class Enemy : MonoBehaviour {

	#region フィールド - Unity Inspector

	public GameObject enemyPrefab;

	public GameObject enemyRoot;

	public GameObject enemyMissilePrefab;

	public Text scoreText;

	public int EnemyHeight;

	public int EnemyWidth;

	public bool IsDestroyedMissileObj = false;

	public bool IsDestroyedEnemyObj = false;

	public EnemyMissile _EnemyMissile;

	public EnemyMissileController _EnemyMissileController;

	public int count = 0;

	public Score _Score;

	public GameObject scoreTextObj;

	//テスト：配列。
	public GameObject[] enemy_test;

	public Title _Title;

	public GameObject titleObj;

	public bool getHighScore = false;

	public AudioClip audioClip1;

	#endregion


	#region フィールド

	private GameObject enemy_dammyPrefab;

	private List<int> intArray;

	private AudioSource audioSource;

	#endregion


	#region メソッド

	void Start () 
	{
		audioSource = gameObject.GetComponent<AudioSource>();
		audioSource.clip = audioClip1;
		audioSource.Play ();

		enemy_test = new GameObject[EnemyHeight * EnemyWidth];

		scoreText.text = 0.ToString ();

		Set ();

		int random_num = 1;
		random_num = Random.Range (0, enemy_test.Length);
		_EnemyMissile = enemy_test [random_num].GetComponent<EnemyMissile> ();
		_EnemyMissile.Set ();
	}

	void Update ()
	{
		_EnemyMissileController = enemyMissilePrefab.GetComponent<EnemyMissileController> ();
	
		//ミサイルオブジェクトor敵オブジェクトがDestroyされたら呼び出し。
		if (IsDestroyedMissileObj || IsDestroyedEnemyObj)
		{
			SetRandomNum ();
			SetMissile ();
		}
	}

	//Enemyオブジェクトの位置の初期化。
	//Enemyオブジェクトの生成。
	private void Set()
	{
		int space = 0;
		int spaceY = 0;

		int num = 0;
		for(int axisY = 0; axisY < EnemyHeight; axisY++)
		{
			space += 40;
			spaceY += 40;

			for (int axisX = 0; axisX < EnemyWidth; axisX++)
			{
				space += 40;
				num += 1;

				enemy_test[num - 1] = Instantiate (enemyPrefab);
				enemy_test[num - 1].transform.SetParent (enemyRoot.transform, false);
				enemy_test[num - 1].transform.localPosition = new Vector3 (enemyRoot.transform.localRotation.x + space, enemyRoot.transform.localRotation.y - spaceY, enemyRoot.transform.localRotation.z);

				if(axisX == EnemyWidth - 1)
				{
					space = 0;
				}
			}
	     }
	}

	public void SetRandomNum()
	{
		intArray = new List<int>();

		for (int i = 0; i < enemy_test.Length; i++)
		{
			if (enemy_test [i] != null) 
			{
				intArray.Add (i);
				int p = Random.Range(0,intArray.Count);
				count = intArray[p];
			} 
		}

	}
		
	public void SetMissile()
	{
		if(enemy_test[count] != null)
		{
			if (enemy_test [count].transform.childCount < 1)
			{
				_EnemyMissile = enemy_test [count].GetComponent<EnemyMissile> ();
				_EnemyMissile.Set (); 

				IsDestroyedMissileObj = false;
				IsDestroyedEnemyObj = false;
			}
	    }
	}
		
	public void SetScoreText()
	{
		_Score = scoreTextObj.GetComponent<Score> ();
		int countObject = enemyRoot.transform.childCount;
	
		_Score.AddPoint(10);

		if (countObject == 0)
		{
			_Score.Save ();

			_Title = titleObj.GetComponent<Title> ();
			_Title.getGameStartbool (false);

			getHighScore = Score.getHighScored ();

			if (getHighScore) 
			{
				SceneManager.LoadScene ("New Records");
			}
			else
			{
				SceneManager.LoadScene ("End-Win");
			}
		}
	}

	#endregion
}
