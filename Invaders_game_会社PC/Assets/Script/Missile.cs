﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Missile : MonoBehaviour {

	#region フィールド　- Unity Inspector

	public float rateY;
	public float limitMoved_Y;

	public bool DeleateMissile;

	#endregion

	//プレイヤーミサイル。
	#region メソッド

	void Start ()
	{
		rateY = 100f;
		limitMoved_Y = 225;

		DeleateMissile = false;
	}

	void Update () 
	{
		SetMissile ();
	}

	public void SetMissile()
	{
		Vector3 pos = transform.localPosition;
	//	Vector3 pos_position = transform.position;
		pos.y += rateY * Time.deltaTime;

		if (pos.y > limitMoved_Y) 
		{
			Destroy (gameObject);
		}
		else
		{
		//	transform.Translate (0, pos.y, 0);
			transform.localPosition = pos;
		}
	}

	private void OnTriggerEnter(Collider other)
	{
		if (other.gameObject.tag == "Enemy")
		{
			Destroy (gameObject);
		}
		if (other.gameObject.tag == "Bonus")
		{
			Destroy (gameObject);
		}
	}

	#endregion
}
