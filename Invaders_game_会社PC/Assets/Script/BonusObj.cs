﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BonusObj : MonoBehaviour {

	#region フィールド - Unity Inspector

	public float moveX_Right;

	public float moveX_Left;

	public bool isChangeObj;

	public GameObject bonusObj;

	public GameObject addBonusObj;

	#endregion

	#region フィールド

	private bool IsMoveRight;

	/// <summary>
	//// Enemyオブジェクト点滅間隔。
	/// </summary>
	private float timeCount = 0.4f;

	/// <summary>
	//// Enemyオブジェクト点滅用判定。
	/// </summary>
	private bool damaged = false;

	private int random_count;

	private AudioSource audioSource;

	#endregion

	void Start () 
	{
		bonusObj.SetActive (false);

		addBonusObj.SetActive (false);

		isChangeObj = false;

	}
	
	void Update ()
	{
		if (isChangeObj)
		{
			addBonusObj.SetActive (true);

			Vector3 pos = bonusObj.transform.localPosition;
			addBonusObj.transform.localPosition = pos;
		}
		else
		{
			StartCoroutine (VisibleObj());
		}

//		if (bonusRigidBody.IsSleeping ())
//		{
//			bonusRigidBody.WakeUp ();
//			Debug.Log ("sleep");
//		}
	

		if (IsMoveRight)
		{
			MoveToStartPoint ();
		} 
		if (!IsMoveRight)
		{
			MoveEnemyObject ();
		}
	}

	//ボーナスオブジェクトを時間差で出現させる。
	private IEnumerator VisibleObj()
	{
		yield return new WaitForSeconds (9f);

		if (!isChangeObj) 
		{
			bonusObj.SetActive (true);

			//{ yield break; }
		}
	}

	// ポイントの追加。
	public void AddBonusBool (bool addBonus)
	{
		isChangeObj = addBonus;
	}

	private void MoveEnemyObject()
	{
		++moveX_Left;
		transform.localPosition = new Vector3 (transform.localPosition.x + (moveX_Left * Time.deltaTime), transform.localPosition.y, transform.localPosition.z);

		if (moveX_Left >= 170)
		{
			IsMoveRight = true;
			MoveToStartPoint ();
			moveX_Left = 0;
		}
	}

	private void MoveToStartPoint()
	{
		++moveX_Right;
		transform.localPosition = new Vector3 (transform.localPosition.x - (moveX_Right * Time.deltaTime), transform.localPosition.y, transform.localPosition.z);

		if (moveX_Right >= 170)
		{
			IsMoveRight = false;
			MoveEnemyObject ();
			moveX_Right = 0;
		}
	}


}
