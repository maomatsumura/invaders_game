﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class Win : MonoBehaviour {

	#region フィールド - Unity Inspector

	// ゲームスタート時。
	public bool getHighScore = false;

	public GameObject normalButton;

	public GameObject enterButton;

	public AudioClip audioClip1;

	#endregion

	#region フィールド 

	private AudioSource audioSource;

	#endregion

	void Start () 
	{
		normalButton.gameObject.SetActive(true);
		enterButton.gameObject.SetActive(false);

		audioSource = gameObject.GetComponent<AudioSource>();
		audioSource.clip = audioClip1;

		//getHighScore = Score.getHighScored ();
		
	}

	void Update () 
	{
//		if (getHighScore) 
//		{
//			SceneManager.LoadScene ("New Records");
//		}		
	}

	public void OnReTryButton()
	{
		audioSource.Play ();

		normalButton.gameObject.SetActive(false);
		enterButton.gameObject.SetActive(true);

		SceneManager.LoadScene ("Title");
	}
}
