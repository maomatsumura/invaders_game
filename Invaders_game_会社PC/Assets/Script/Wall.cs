﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Wall : MonoBehaviour {

	#region フィールド - Unity Inspector

	public GameObject wallRootPrefab;

	//オブジェクトが複数集まって出来ている壁オブジェクトを作成。
	public List<GameObject> wallRoot;

	//オブジェクトの集合体数。
	public int wallRootNumber = 4;

	#endregion

	#region フィールド
	private GameObject wall;

	#endregion

	#region メソッド

	void Start ()
	{
		CreateWallObject ();

	}

	void Update ()
	{
		
	}

	private void CreateWallObject()
	{
		int x = 0;
		int y = 0;

		for(int i = 0; i < wallRootNumber; i++)
		{
			wall = Instantiate (wallRoot[i]);
			wall.transform.SetParent (wallRootPrefab.transform);

			if (i == 1) 
			{
				x = 20;
				y = 0;
			} 
			if (i == 2)
			{
				x = 0;
				y = -20;
			}
			if (i == 3)
			{
				x = 20;
				y = -20;
			}
			wall.transform.localPosition = new Vector3 (wallRoot [i].transform.localPosition.x + x, wallRoot[i].transform.localPosition.y + y,  wallRoot[i].transform.localPosition.z + 10);
			wall.transform.localScale = new Vector3(wallRoot [i].transform.localScale.x + 10, wallRoot[i].transform.localScale.y + 10,  wallRoot[i].transform.localScale.z +10);
		}
	}

	#endregion
}