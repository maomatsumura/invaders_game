﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Title : MonoBehaviour {

	#region フィールド - Unity Inspector

	// ゲームスタート時。
	public static bool gameStart;

	public GameObject normalButton;

	public GameObject enterButton;

	public AudioClip audioClip1;

	#endregion

	#region フィールド

	private AudioSource audioSource;

	#endregion

	#region メソッド

	void Start () 
	{
		normalButton.gameObject.SetActive(true);
		enterButton.gameObject.SetActive(false);

		gameStart = true;
	}

	void Update () 
	{
		
	}

	public void OnStartButton()
	{
		audioSource = gameObject.GetComponent<AudioSource>();
		audioSource.clip = audioClip1;
		audioSource.Play ();

		normalButton.gameObject.SetActive(false);
		enterButton.gameObject.SetActive(true);

		SceneManager.LoadScene ("CountDown");
	}

	public static bool getGameStart()
	{
		return gameStart;
	}

	public void getGameStartbool(bool get)
	{
		gameStart = get;
	}

	#endregion
}
