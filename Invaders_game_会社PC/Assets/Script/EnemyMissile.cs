﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyMissile : MonoBehaviour {

	#region フィールド - Unity Inspector
	public GameObject enemy_misslePrefab;

	public GameObject enemy_playerObject;

	//ミサイル発射回数。
	public int	MissileCount;

	//ミサイル発射最大回数。
	public int MaxMissile = 1;

	#endregion

	#region フィールド

	private GameObject enemy_dammyPrefab;

	#endregion

	#region メソッド

	void Start ()
	{

	}

	void Update ()
	{

	}
		
	public void Set()
	{
		enemy_dammyPrefab = Instantiate (enemy_misslePrefab);
		enemy_dammyPrefab.transform.SetParent (enemy_playerObject.transform);
		enemy_dammyPrefab.transform.localPosition = new Vector3 (0, 0, 0);
	}

	#endregion
}
