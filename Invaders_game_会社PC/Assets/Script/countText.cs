﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


public class countText : MonoBehaviour {

	#region フィールド - Unity Inspector

	public GameObject countWall;

	public Text countNum;

	public AudioClip audioClip1;

	public AudioClip audioClip2;

	#endregion

	#region フィールド

	private float timeCount = 5f;

	private AudioSource audioSource;

	#endregion

	void Start () 
	{

	}

	void Update ()
	{
		timeCount -= Time.deltaTime;

		audioSource = gameObject.GetComponent<AudioSource>();
		audioSource.clip = audioClip1;

		//soundCount ();

		if (timeCount < 5 && timeCount > 3)
		{
			//soundCount ();

			countNum.text = "3";
		}
		if (timeCount < 3.5 && timeCount > 2) 
		{
			//soundCount ();

			countNum.text = "2";
		} 
		if (timeCount < 2 && timeCount > 1) 
		{
			//soundCount ();

			countNum.text = "1";
		}
		if (timeCount < 0.5) 
		{
			SoundGo ();

			countNum.text = "GO!";

			StartCoroutine (SceneLoad());
		}
	}

	private void soundCount()
	{
		for (int i = 0; i < 3; i++) {
			audioSource = gameObject.GetComponent<AudioSource> ();
			audioSource.clip = audioClip1;
			audioSource.Play ();

			if (audioSource.time > 2) {
				audioSource.Stop ();
			}
		}
	}

	private void SoundGo()
	{
		audioSource = gameObject.GetComponent<AudioSource>();
		audioSource.clip = audioClip2;
		audioSource.Play ();
	}

	private IEnumerator SceneLoad()
	{
		yield return new WaitForSeconds (1.0f);

		SceneManager.LoadScene ("Main");
	}
}
