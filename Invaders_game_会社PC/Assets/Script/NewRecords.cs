﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class NewRecords : MonoBehaviour {

	#region フィールド - Unity Inspector

	public  bool getHighScore;

	public  bool getLose;

	public Score _Score;

	public GameObject scoreObj;

	#endregion

	void Start () 
	{
		
	}

	void Update ()
	{
		StartCoroutine (CloseScene());
	}

	public IEnumerator CloseScene()
	{
		yield return new WaitForSeconds (2.0f);

		_Score = scoreObj.GetComponent<Score> ();
		_Score.getHighScoredbool(false);

		getLose = PlayerController.getLose;

		if (getLose)
		{
			SceneManager.LoadScene ("End");
		}
		else
		{
			SceneManager.LoadScene ("End-Win");
		}
	}


}
