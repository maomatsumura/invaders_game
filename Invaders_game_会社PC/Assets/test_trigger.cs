﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class test_trigger : MonoBehaviour {

	public Collider testCollider;

	public GameObject testRootPrefab;

	public GameObject testRoot;

	private GameObject test;

	// Use this for initialization
	void Start ()
	{
		testCollider.isTrigger = true;

		SetTest ();
	}
	
	// Update is called once per frame
	void Update () 
	{
		
	}

	private void SetTest()
	{
		test = Instantiate (testRoot);
		test.transform.SetParent (testRootPrefab.transform);

		test.transform.localPosition = new Vector3 (testRoot.transform.localPosition.x , testRoot.transform.localPosition.y, testRoot.transform.localPosition.z);
	}

	private void OnTriggerEnter(Collider other)
	{
		if(other.gameObject.tag == "Missile")
		{
			Debug.Log ("hit test");
			//			isDestroyedObj = true;
			//			SetBool ();
		}
	}
}
